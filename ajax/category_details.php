<?php	
include("config.php");

$select_category = mysqli_query($db , "select * from category");
$displayheader = '<div class=\"header-top-bar\"></div> 
		<nav class=\"header-menu col-md-12\"><!--MENU START-->
							
<div id=\"mega_main_menu\" class=\"mega_main_menu nav_menu mega_main_sidebar_menu primary_style-flat icons-left first-lvl-align-right first-lvl-separator-none language_direction-ltr direction-horizontal responsive-enable mobile_minimized-enable dropdowns_animation-anim_3 version-1-1-4 include-logo no-search include-woo_cart no-buddypress\">
	<div class=\"menu_holder\" data-sticky=\"1\" data-stickyoffset=\"100\">
	<div class=\"fullwidth_container\"></div><!-- class=\"fullwidth_container\" -->
		<div class=\"menu_inner\">
			<span class=\"nav_logo\">
				<a class=\"logo_link\" href=\"http://herowp.com/demos/myway\" title=\"MyWay Wordpress Theme Demo\"><img src=\"http://herowp.com/demos/myway/wp-content/uploads/2014/08/logo.png\" alt=\"MyWay Wordpress Theme Demo\" /></a>
				<a class=\"mobile_toggle\">
					<span class=\"mobile_button\">
						Menu Responsive &nbsp;
						<span class=\"symbol_menu\">&equiv;</span>
						<span class=\"symbol_cross\">&#x2573;</span>
					</span><!-- class=\"mobile_button\" -->
				</a>
			</span><!-- /class=\"nav_logo\" -->
				<ul id=\"mega_main_menu_ul\" class=\"mega_main_menu_ul\">
<li id=\"menu-item-6\" class=\"menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6 multicolumn_dropdown default_style drop_to_center submenu_full_width columns6\">
	<a href=\"#\" class=\"item_link  with_icon\">
		<i class=\"im-icon-home-4\"></i> 
		<span>
			<span class=\"link_text\">
			Home
			</span>
		</span>
	</a>
</li>
<li id=\"menu-item-108\" class=\"menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor current-menu-parent menu-item-has-children menu-item-108 default_dropdown default_style drop_to_right submenu_default_width columns1\">
	<a href=\"#\" class=\"item_link  with_icon\">
		<i class=\"im-icon-briefcase\"></i> 
		<span>
			<span class=\"link_text\">
			Gallary
			</span>
		</span>
	</a>
	<ul class=\"mega_dropdown\">';

while($fetch_category = mysqli_fetch_array($select_category)){
$displayheader .=	'<li id="menu-item-110" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-110 default_dropdown default_style drop_to_right submenu_default_width columns">
		<a href="categorydetails.html?id=';
$displayheader .= $fetch_category['id'];		
$displayheader .=	'" class="item_link  with_icon">
			<i class="im-icon-align-left"></i> 
			<span>
				<span class="link_text">';
$displayheader .=		  $fetch_category['name'];
$displayheader .='	</span>
			</span>
		</a>
	</li>';
}
	
$displayheader .=	'
	</ul><!-- /.mega_dropdown -->
</li>
<li id=\"menu-item-67\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-67 default_dropdown default_style drop_to_right submenu_default_width columns1\">
	<a href=\"#\" class=\"item_link  with_icon\">
		<i class=\"fa-icon-edit\"></i> 
		<span>
			<span class=\"link_text\">
			About Us
			</span>
		</span>
	</a>
</li>
<li id=\"menu-item-101\" class=\"menu-item menu-item-type-post_type menu-item-object-page menu-item-101 default_dropdown default_style drop_to_right submenu_default_width columns1\">
	<a href=\"#\" class=\"item_link  with_icon\">
		<i class=\"fa-icon-envelope\"></i> 
		<span>
			<span class=\"link_text\">
			Contact us
			</span>
		</span>
	</a>
</li>
</ul>
		</div><!-- /class=\"menu_inner\" -->
	</div><!-- /class=\"menu_holder\" -->
</div><!-- /id=\"mega_main_menu\" -->';

echo  $displayheader;
?>