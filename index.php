<?php
include("ajax/config.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="msapplication-tap-highlight" content="no" />
        <!-- WARNING: for iOS 7, remove the width=device-width and height=device-height attributes. See https://issues.apache.org/jira/browse/CB-4323 -->
        <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi" />
        <link rel='stylesheet' id='aqpb-view-css-css'  href='css/aqpb-view.css' media='all' />
<link rel='stylesheet' id='mmpm_mega_main_menu-css'  href='css/cache.skin.css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='css/styles.css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='css/settings.css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link rel='stylesheet' id='reset-css'  href='css/reset.css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='css/bootstrap.css' media='all' />
<link rel='stylesheet' id='style-css'  href='css/style.css' media='all' />
<link rel='stylesheet' id='responsive-css'  href='css/responsive.css' media='all' />
<link rel='stylesheet' id='fontawesome-css'  href='css/font-awesome.min.css' media='all' />
<link rel='stylesheet' id='colorpicker-css'  href='css/colpick.css' media='all' />
<link rel='stylesheet' id='pe-7s-css-css'  href='css/pe-icon-7-stroke.css' media='all' />
<link rel='stylesheet' id='pe-7s-css-filled-css'  href='css/pe-icon-7-filled.css' media='all' />
<link rel='stylesheet' id='custom-font-icons-css-css'  href='css/style-custom-font-icons.css' media='all' />
<link rel='stylesheet' id='touchtouch-css'  href='css/touchTouch.css' media='all' />
<link rel='stylesheet' id='animate-css'  href='css/animate.css' media='all' />
<link rel='stylesheet' id='YTPlayer-css'  href='css/YTPlayer.css' media='all' />
<link rel='stylesheet' id='hover-css'  href='css/hover-min.css' media='all' />
<link rel='stylesheet' id='herowpfont-css'  href='css/herowpfont.css' media='all' />
<link rel='stylesheet' id='lightbox-css'  href='css/lightbox.css' media='all' />
<link rel='stylesheet' id='mmpm_icomoon-css'  href='css/icomoon.css' media='all' />
<link rel='stylesheet' id='mmpm_font-awesome-css'  href='css/font-awesome.css' media='all' />
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='js/colpick.js'></script>

<!--[if gte IE 9]>
	<style type="text/css">
		.#mega_main_menu,
		.#mega_main_menu *
		{
			filter: none;
		}
	</style>
<![endif]-->

        <title>Memories of Journey</title>
    </head>
    <body class="page page-id-9 page-template page-template-home-page page-template-home-page-php homepage" onload="ajaxcall()" oncontextmenu="return false">
        <div  class="main"  id="wrap-home"><!--WRAP boxed/full START-->
	<header class="home"><!--HEADER START-->
	<div class="container" id="papan"><!--CONTAINER HEADER START-->

					</nav><!--MENU END--> 
    </div><!--CONTAINER HEADER END-->
    </header><!--HEADER END-->



	
<div id="aq-template-wrapper-509" class=""><!--AQ TEMPLATE WRAPPER-509 START-->
      <div id="aq-block-509-1" class="col-md-12 nopadding"><!--AQ BLOCK-509-1 START-->							
						
	
	<!-- START REVOLUTION SLIDER 4.6.0 fullwidth mode -->

<div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;background-color:#FFFFFF;padding:0px;margin-top:0px;margin-bottom:0px;max-height:937px;">
	<div id="rev_slider_2_1" class="rev_slider fullwidthabanner" style="display:none;max-height:937px;height:937px;">
<ul>	

<?php
$select_slider = mysqli_query($db , "select * from slider order by id desc");
while($fetch_slider = mysqli_fetch_array($select_slider))
{
?>

<!-- SLIDE  -->
	<li data-transition="zoomin" data-slotamount="7" data-masterspeed="1500"  data-saveperformance="off" >
		<!-- MAIN IMAGE -->
		<img src="admin/<?php echo $fetch_slider['horizontal']; ?>"  alt="slide-1"  data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" style="width:1920px;height:938px;">
	</li>
	<!-- SLIDE  -->

<?php } ?>


</ul>
<div class="tp-bannertimer"></div>	</div>

			<script type="text/javascript">

				/******************************************
					-	PREPARE PLACEHOLDER FOR SLIDER	-
				******************************************/
				

				var setREVStartSize = function() {
					var	tpopt = new Object();
						tpopt.startwidth = 1220;
						tpopt.startheight = 937;
						tpopt.container = jQuery('#rev_slider_2_1');
						tpopt.fullScreen = "off";
						tpopt.forceFullWidth="off";

					tpopt.container.closest(".rev_slider_wrapper").css({height:tpopt.container.height()});tpopt.width=parseInt(tpopt.container.width(),0);tpopt.height=parseInt(tpopt.container.height(),0);tpopt.bw=tpopt.width/tpopt.startwidth;tpopt.bh=tpopt.height/tpopt.startheight;if(tpopt.bh>tpopt.bw)tpopt.bh=tpopt.bw;if(tpopt.bh<tpopt.bw)tpopt.bw=tpopt.bh;if(tpopt.bw<tpopt.bh)tpopt.bh=tpopt.bw;if(tpopt.bh>1){tpopt.bw=1;tpopt.bh=1}if(tpopt.bw>1){tpopt.bw=1;tpopt.bh=1}tpopt.height=Math.round(tpopt.startheight*(tpopt.width/tpopt.startwidth));if(tpopt.height>tpopt.startheight&&tpopt.autoHeight!="on")tpopt.height=tpopt.startheight;if(tpopt.fullScreen=="on"){tpopt.height=tpopt.bw*tpopt.startheight;var cow=tpopt.container.parent().width();var coh=jQuery(window).height();if(tpopt.fullScreenOffsetContainer!=undefined){try{var offcontainers=tpopt.fullScreenOffsetContainer.split(",");jQuery.each(offcontainers,function(e,t){coh=coh-jQuery(t).outerHeight(true);if(coh<tpopt.minFullScreenHeight)coh=tpopt.minFullScreenHeight})}catch(e){}}tpopt.container.parent().height(coh);tpopt.container.height(coh);tpopt.container.closest(".rev_slider_wrapper").height(coh);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);tpopt.container.css({height:"100%"});tpopt.height=coh;}else{tpopt.container.height(tpopt.height);tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);}
				};

				/* CALL PLACEHOLDER */
				setREVStartSize();


				var tpj=jQuery;
				tpj.noConflict();
				var revapi2;

				tpj(document).ready(function() {

				if(tpj('#rev_slider_2_1').revolution == undefined)
					revslider_showDoubleJqueryError('#rev_slider_2_1');
				else
				   revapi2 = tpj('#rev_slider_2_1').show().revolution(
					{
						dottedOverlay:"none",
						delay:4000,
						startwidth:1220,
						startheight:937,
						hideThumbs:200,

						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:2,
						
												
						simplifyAll:"off",

						navigationType:"none",
						navigationArrows:"solo",
						navigationStyle:"round",

						touchenabled:"on",
						onHoverStop:"on",
						nextSlideOnWindowFocus:"off",

						swipe_threshold: 0.7,
						swipe_min_touches: 1,
						drag_block_vertical: false,
						
												parallax:"mouse+scroll",
						parallaxBgFreeze:"off",
						parallaxLevels:[-5,-10,-15,-20,-25,-30,-35,-40,-45,-50],
												parallaxDisableOnMobile:"on",
												
												
						keyboardNavigation:"off",

						navigationHAlign:"center",
						navigationVAlign:"bottom",
						navigationHOffset:0,
						navigationVOffset:20,

						soloArrowLeftHalign:"left",
						soloArrowLeftValign:"center",
						soloArrowLeftHOffset:20,
						soloArrowLeftVOffset:0,

						soloArrowRightHalign:"right",
						soloArrowRightValign:"center",
						soloArrowRightHOffset:20,
						soloArrowRightVOffset:0,

						shadow:0,
						fullWidth:"on",
						fullScreen:"off",

						spinner:"spinner0",
						
						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off",

						autoHeight:"off",
						forceFullWidth:"off",
						
						
						
						hideThumbsOnMobile:"off",
						hideNavDelayOnMobile:1500,
						hideBulletsOnMobile:"off",
						hideArrowsOnMobile:"off",
						hideThumbsUnderResolution:0,

												hideSliderAtLimit:0,
						hideCaptionAtLimit:0,
						hideAllCaptionAtLilmit:0,
						startWithSlide:0					});



					
				});	/*ready*/

			</script>


			</div><!-- END REVOLUTION SLIDER -->
      </div><!--AQ BLOCK-509-1 END-->
	  
	  
<div class="custom-column-background" style="background: url('') no-repeat; background-color:#fff; background-position:center;;"><!--CUSTOM COLUMN BACKGROUND START--><div class="container"><!--CONTAINER START-->
      <div id="aq-block-509-2" class="col-md-12 nopadding"><!--AQ BLOCK-509-2 START-->
      <div id="aq-block-509-3" class="col-md-12" style="padding:0"><!--AQ BLOCK-509-3 START-->							
<style type="text/css">
@media only screen and (min-width : 120px) and (max-width : 320px)
{
	#herowp-block-id-oQUKHKZgz-864430533	
	{
		margin-top:-90px !important;margin-bottom:165px !important;	
	}
	
}
@media only screen and (min-width : 321px) and (max-width : 480px) 
{
	#herowp-block-id-oQUKHKZgz-864430533	
	{
		margin-top:-90px !important;margin-bottom:165px !important;	
	}
}
@media only screen and (min-width : 481px) and (max-width : 768px) 
{
	#herowp-block-id-oQUKHKZgz-864430533	
	{
		margin-top:-130px !important;margin-bottom:165px !important;	
	}
}	
@media only screen and (min-width : 769px) and (max-width : 960px) 
{
	#herowp-block-id-oQUKHKZgz-864430533	
	{
		margin-top:-170px !important;margin-bottom:165px !important;	
	}
}	
</style>
						
	
				
			<div id="herowp-block-id-oQUKHKZgz-864430533" class="heading" style="margin-top:-320px; margin-bottom:px;"><!--HEADING START-->			
						
			<div class="top_headings"><!--top_headings START-->
                     <div class="col-lg-12"><!--col-lg-12 START-->
                                <div class="title-heading">
										<h1  style="color:brown; font-family:0; text-transform:0;  text-align:left; font-size:42px">What we <span style="color:white; font-family:0; text-transform:8;">Love to do</span></h1>
															<p class="heading" style="color:white; text-transform:8;  font-family:0;  ; text-align:left">Be sure that we know to do a lot</p>
									                                </div>
                    </div><!--col-lg-12 END-->
				</div><!--top_headings END-->
			
			
		</div><!--HEADING END-->
      </div><!--AQ BLOCK-509-3 END-->
	  
	  
      <div id="aq-block-509-4" class="col-md-12" style="padding:0"><!--AQ BLOCK-509-4 START-->							
						
	
				
			<div  class="services_home10" style="margin-top:-180px; margin-bottom:80px;"><!--SERVICES 10 START-->		
								<div class="col-md-4 col-sm-6 col-xs-12 "><!--COLS SERVICES START-->
									<div class="box-services" ><!--box-services START--><span class="servicesnumber " ><i class="icon-approved-window"></i></span><div class="services-box"><h2 class="" style="color:#4f4f4f">refreshing design</h2><p class="" style="color:#353535">Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.</p>
								
															</div><!--services-box-->
									</div><!--box-services END-->
								</div><!--COLS SERVICES START-->
								
								
								<div class="col-md-4 col-sm-6 col-xs-12 "><!--COLS SERVICES START-->
									<div class="box-services" ><!--box-services START--><span class="servicesnumber " ><i class="icon-administrator"></i></span><div class="services-box"><h2 class="" style="color:#4f4f4f">stay awake</h2><p class="" style="color:#353535">Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.</p>
								
															</div><!--services-box-->
									</div><!--box-services END-->
								</div><!--COLS SERVICES START-->
								
								
								<div class="col-md-4 col-sm-6 col-xs-12 "><!--COLS SERVICES START-->
									<div class="box-services" ><!--box-services START--><span class="servicesnumber " ><i class="icon-code-window"></i></span><div class="services-box"><h2 class="" style="color:#4f4f4f">code with love</h2><p class="" style="color:#353535">Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.</p>
								
															</div><!--services-box-->
									</div><!--box-services END-->
								</div><!--COLS SERVICES START-->
								
								
								<div class="col-md-4 col-sm-6 col-xs-12 "><!--COLS SERVICES START-->
									<div class="box-services" ><!--box-services START--><span class="servicesnumber " ><i class="icon-casette-tape"></i></span><div class="services-box"><h2 class="" style="color:#4f4f4f">Hispter cool</h2><p class="" style="color:#353535">Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.</p>
								
															</div><!--services-box-->
									</div><!--box-services END-->
								</div><!--COLS SERVICES START-->
								
								
								<div class="col-md-4 col-sm-6 col-xs-12 "><!--COLS SERVICES START-->
									<div class="box-services" ><!--box-services START--><span class="servicesnumber " ><i class="icon-cursor-click2"></i></span><div class="services-box"><h2 class="" style="color:#4f4f4f">From the heart</h2><p class="" style="color:#353535">Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.</p>
								
															</div><!--services-box-->
									</div><!--box-services END-->
								</div><!--COLS SERVICES START-->
								
								
								<div class="col-md-4 col-sm-6 col-xs-12 "><!--COLS SERVICES START-->
									<div class="box-services" ><!--box-services START--><span class="servicesnumber " ><i class="icon-edit-map"></i></span><div class="services-box"><h2 class="" style="color:#4f4f4f">Easy to use</h2><p class="" style="color:#353535">Lorem ipsum dolor sit amet, consectetur adipiscing elit est is.</p>
								
															</div><!--services-box-->
									</div><!--box-services END-->
								</div><!--COLS SERVICES START--></div><!--SERVICES 10 END-->
      </div><!--AQ BLOCK-509-4 END-->
	  
	  </div><!--CONTAINER END--></div><!--CUSTOM COLUMN BACKGROUND END-->


      </div><!--AQ BLOCK-509-2 END-->




<div class="custom-column-background" style="background: url('images/stripes.png') repeat; background-color:#fff; background-position:top center;border-bottom:solid 1px #dddddd;;"><!--CUSTOM COLUMN BACKGROUND START--><div class="container"><!--CONTAINER START-->
      <div id="aq-block-509-10" class="col-md-12 nopadding"><!--AQ BLOCK-509-10 START-->
      <div id="aq-block-509-11" class="col-md-12" style="padding:0"><!--AQ BLOCK-509-11 START-->							
						
	
				
			<div  class="heading" style="margin-top:60px; margin-bottom:-10px;"><!--HEADING START-->			
						
			<div class="top_headings"><!--top_headings START-->
                     <div class="col-lg-12"><!--col-lg-12 START-->
                                <div class="title-heading">
									<h1  style="color:#4e4e4e; font-family:0; text-transform:0;  text-align:left; font-size:42px">We are proud <span style="color:#4e4e4e; font-family:0; text-transform:0;">of our work</span></h1>
										<p class="heading" style="color:#4e4e4e; text-transform:0;  font-family:0;  ; text-align:left">Browse the hard work.</p>
									                                </div>
                    </div><!--col-lg-12 END-->
				</div><!--top_headings END-->
			
			
		</div><!--HEADING END-->
      </div><!--AQ BLOCK-509-11 END-->
	  
	  
      <div id="aq-block-509-12" class="col-md-12" style="padding:0"><!--AQ BLOCK-509-12 START-->							
						
	
	
    
<div  class="portfolio_block1" style="margin-top:0px; margin-bottom:60px;"><!--portfolio_block1 START-->



				
			
				
    <div class="portfolio_inner"><!--portfolio_inner START-->
    			
    		
		  
<?php
$select_category = mysqli_query($db , "SELECT * FROM `category` WHERE `name` like 'photography'");
$fetch_category = mysqli_fetch_array($select_category);
$select_subcategory = mysqli_query($db , "select * from subcategory where catid = '".$fetch_category['id']."' order by RAND() desc limit 6");
$image_link;
$Subcategory_name;
$Subcategory_id;
while($fetch_subcategory = mysqli_fetch_array($select_subcategory))
{
	$select_image = mysqli_query($db , "SELECT * FROM `photo` WHERE `subcatid` = '".$fetch_subcategory['id']."' order by id desc");
	$fetch_image = mysqli_fetch_array($select_image);

?>          

          

		
	 
		  <div class="col-md-4 portfolio-box" ><!--PORTFOLIO POST START-->
                            <section class="mix planning"><!--section START-->
                               
					

							   <div class="port_img overlay-image view-overlay"><!--port_img START-->	
                                                                            
                                            <img src="admin/<?php echo $fetch_image['image'];?>" alt="<?php echo $fetch_subcategory['name'] ; ?>" class="img-responsive" style="width:377px;height:260px;"/>                                            
                                        					
                                            <div class="mask">
                        						<div class="port-zoom-link height62">
                                                	  <a href="eachcategorydetails.html?id=<?php echo $fetch_subcategory['id']; ?>"><i class="fa fa-link"></i></a>
                                                </div>
                   							 </div><!--MASK END-->
                                                                    </div><!--port_img END-->
                              <div class="project_date_added_and_category">
                                  <h2><a href="eachcategorydetails.html?id=<?php echo $fetch_subcategory['id']; ?>"><?php echo $fetch_subcategory['name']; ?><span class="dotcolor"> .</span></a></h2>
								   <div class="separator"></div>
								  <p class="category"></p>
                                </div>
                                      
                       </section><!--section END-->
		  </div><!--PORTFOLIO POST END-->
		
		
    
	

<?php } ?>		
       
		  
          

		
	 
	
		
    
	

		
                   
	</div><!--portfolio_inner END-->	
</div><!--portfolio_block1 END-->

      </div><!--AQ BLOCK-509-12 END-->
	  
	  </div><!--CONTAINER END--></div><!--CUSTOM COLUMN BACKGROUND END-->


      </div><!--AQ BLOCK-509-10 END-->

<?php include("whyuchooseus.php")?>


     <div id="aq-block-509-29" class="col-md-12 nopadding"><!--AQ BLOCK-509-29 START-->							
						
	
				
			<div  class="heading" style="margin-top:40px; margin-bottom:35px;"><!--HEADING START-->			
						
			<div class="top_headings"><!--top_headings START-->
                     <div class="col-lg-12"><!--col-lg-12 START-->
                                <div class="title-heading">
									<h1  style="color:#353535; font-family:0; text-transform:0;  text-align:center; font-size:42px">prices for  <span style="color:#3f3f3f; font-family:0; text-transform:0;">our work.</span></h1>
									<p class="heading" style="color:#5b5b5b; text-transform:0;  font-family:0; padding:0 25%; ; text-align:center">Yes there are plenty. But you don&#39;t have to believe, just browse!</p>
									                                </div>
                    </div><!--col-lg-12 END-->
				</div><!--top_headings END-->
			
			
		</div><!--HEADING END-->
      </div><!--AQ BLOCK-509-29 END-->

<?php
$select_price_details = mysqli_query($db , "select * from price_table order by amount");
$i=0;
$title;
$amount;
$details;

while($select_price_fetch = mysqli_fetch_array($select_price_details))
{
	$title[$i] = $select_price_fetch['title'];
	$amount[$i] = $select_price_fetch['amount'];
	$details[$i] = $select_price_fetch['details'];

	$i++;
}
?>	  

	  
<div class="slide" id="slide-509-30" style="background:url(http://herowp.com/demos/myway/wp-content/uploads/2014/09/graphic-design-desktop-wallpaper-7.jpg) center no-repeat;   display:table;" data-stellar-background-ratio="0.12"><!--SLIDE-509-30 START--><div style="position: absolute;
height: 100%;width: 100%; background:url() repeat; background-attachment:fixed;"></div>
	<div class="container"><!--CONTAINER START-->
      <div id="aq-block-509-30" class="col-md-12 nopadding"><!--AQ BLOCK-509-30 START-->
      <div id="aq-block-509-31" class="col-md-12" style="padding:0"><!--AQ BLOCK-509-31 START-->							
						
	
				
			<div  class="pricing_tables" style="margin-top:70px; margin-bottom:50px;"><!--PRICING START-->			
												
																				
																				
																				
																				
																				
																				
																				
										<div class="pricing_tables_holder"><!--PRICING TABLES HOLDER START-->
								
								
								<div class="col-md-3 col-sm-6 col-xs-12"><!--PRICING TABLE END -->
											
									<div class="box-pricing " style="border-color:"><!--BOX PRICING START--><p class="plan_icon"><i class="fa fa-paper-plane" style="color:"></i></p><h1 style="color:"><?php echo $title[0]; ?><span class="dotcolor"></h1>
										
										<div class="price_circle" style="width:90%;"><!--PRICE CIRCLE START-->
											<div class="price_circle_inner"><!--PRICE CIRCLE INNER START-->
												<h1 class="price"><?php echo $amount[0]; ?></h1>
											</div><!--PRICE CIRCLE INNER END-->
												<p class="billing">INR</p>
										</div><!--PRICE CIRCLE END-->
										
										<div class="line" style="background:"></div>
										
										<ul style="color:">
											<?php echo $details[0]; ?>
										</ul>
										
										<h2 class="buy"><a href="#" class=""><i class="fa fa-paper-plane"></i>BOOK NOW</a></h2>

									</div><!--BOX PRICING END-->
									
									
								
								</div><!--PRICING TABLE END -->
								
								
								
								<div class="col-md-3 col-sm-6 col-xs-12"><!--PRICING TABLE END -->
											
									<div class="box-pricing " style="border-color:"><!--BOX PRICING START--><p class="plan_icon"><i class="fa fa-refresh" style="color:"></i></p><h1 style="color:"><?php echo $title[1]; ?><span class="dotcolor"></h1>
										
										<div class="price_circle" style="width:90%;"><!--PRICE CIRCLE START-->
											<div class="price_circle_inner"><!--PRICE CIRCLE INNER START-->
												<h1 class="price"><?php echo $amount[1]; ?></h1>
											</div><!--PRICE CIRCLE INNER END-->
												<p class="billing">INR</p>
										</div><!--PRICE CIRCLE END-->
										
										<div class="line" style="background:"></div>
										
										<ul style="color:">
											<?php echo $details[1]; ?>
										</ul>
										
										<h2 class="buy"><a href="#" class=""><i class="fa fa-paper-plane"></i>BOOK NOW</a></h2>

									</div><!--BOX PRICING END-->
									
									
								
								</div><!--PRICING TABLE END -->
								
								
								
								<div class="col-md-3 col-sm-6 col-xs-12"><!--PRICING TABLE END -->
											
									<div class="box-pricing featured_pricing" style="border-color:"><!--BOX PRICING START--><p class="plan_icon"><i class="fa fa-leaf" style="color:"></i></p><h1 style="color:"><?php echo $title[2]; ?><span class="dotcolor"></h1>
										
										<div class="price_circle" style="width:90%;"><!--PRICE CIRCLE START-->
											<div class="price_circle_inner"><!--PRICE CIRCLE INNER START-->
												<h1 class="price"><?php echo $amount[2]; ?></h1>
											</div><!--PRICE CIRCLE INNER END-->
												<p class="billing">INR</p>
										</div><!--PRICE CIRCLE END-->
										
										<div class="line" style="background:"></div>
										
										<ul style="color:">
											<?php echo $details[2]; ?>
										</ul>
										
										<h2 class="buy"><a href="#" class="featured_plan"><i class="fa fa-paper-plane"></i>BOOK NOW</a></h2>

									</div><!--BOX PRICING END-->
									
									
								
								</div><!--PRICING TABLE END -->
								
								
								
								<div class="col-md-3 col-sm-6 col-xs-12"><!--PRICING TABLE END -->
											
									<div class="box-pricing " style="border-color:"><!--BOX PRICING START--><p class="plan_icon"><i class="fa fa-cloud" style="color:"></i></p><h1 style="color:"><?php echo $title[3]; ?><span class="dotcolor"></h1>
										
										<div class="price_circle" style="width:90%;"><!--PRICE CIRCLE START-->
											<div class="price_circle_inner"><!--PRICE CIRCLE INNER START-->
												<h1 class="price"><?php echo $amount[3]; ?></h1>
											</div><!--PRICE CIRCLE INNER END-->
												<p class="billing">INR</p>
										</div><!--PRICE CIRCLE END-->
										
										<div class="line" style="background:"></div>
										
										<ul style="color:">
											<?php echo $details[3]; ?>
										</ul>
										
										<h2 class="buy"><a href="#" class=""><i class="fa fa-paper-plane"></i>BOOK NOW</a></h2>

									</div><!--BOX PRICING END-->
									
									
								
								</div><!--PRICING TABLE END -->
								
	</div><!--PRICING TABLES HOLDER END-->
</div><!--PRICING END-->
      </div><!--AQ BLOCK-509-31 END-->
	  
	  
      </div><!--AQ BLOCK-509-30 END-->
	  
	  
    </div><!--CONTAINER END-->
</div><!--SLIDE-509-38 END-->

  
<div class="custom-column-background" style="background: url('http://herowp.com/demos/myway/wp-content/uploads/2014/08/pattern1.png') repeat; background-color:#fff; background-position:top center;;"><!--CUSTOM COLUMN BACKGROUND START--><div class="container"><!--CONTAINER START-->
      <div id="aq-block-509-35" class="col-md-12 nopadding"><!--AQ BLOCK-509-35 START-->
      <div id="aq-block-509-36" class="col-md-12" style="padding:0"><!--AQ BLOCK-509-36 START-->							
						
	
				
			<div  class="heading" style="margin-top:50px; margin-bottom:10px;"><!--HEADING START-->			
						
			<div class="top_headings"><!--top_headings START-->
                     <div class="col-lg-12"><!--col-lg-12 START-->
                                <div class="title-heading">
																			<h1  style="color:#4e4e4e; font-family:0; text-transform:0;  text-align:left; font-size:42px">meet the super <span style="color:#4e4e4e; font-family:0; text-transform:0;"> team.</span></h1>
																												<p class="heading" style="color:#4e4e4e; text-transform:0;  font-family:0;  ; text-align:left">Be sure that we know to do a lot</p>
									                                </div>
                    </div><!--col-lg-12 END-->
				</div><!--top_headings END-->
			
			
		</div><!--HEADING END-->
      </div><!--AQ BLOCK-509-36 END-->
	  
	  
      <div id="aq-block-509-37" class="col-md-12" style="padding:0"><!--AQ BLOCK-509-37 START-->							
						
	
	
	<div  class="team-wrap" style="margin-top:0px; margin-bottom:80px;"><!--TEAM WRAP START-->
	
				<div class="col-md-4 col-sm-4 col-xs-12"><!--COLS START-->
					<div class="team-boxes not-animated" data-animate="fadeInUp" data-delay="0"><!--TEAM BOXES START-->
                        <div class="team-thumb overlay-image view-overlay"><!--TEAM THUMB START--><img src="http://herowp.com/demos/myway/wp-content/uploads/2014/09/ca3ef2574ff11496a0e6857135f9013d-377x301.jpg" alt="Homepage"><div class="clear"></div>
						<div class="mask team_quote"><!--MASK START-->
                        						<div class="port-zoom-link height80"><!--port-zoom-link START-->
                                                	  <h2>"Imagination is more important then knowledge."</h2>
													  <p>Albert Einstein</p>
                                                </div><!--port-zoom-link END-->
                   							 </div><!--MASK END-->
                        </div><!--TEAM THUMB END-->
							<div class="team-info"><!--TEAM INFO START-->
									<h2>Arthur Michael Park</h2>
									<p>Webdesigner</p>
							</div><!--TEAM INFO END-->
							<div class="separator"></div>
							<div class="team-social"><!--TEAM SOCIAL START-->
									<a href=""><i class="fa fa-facebook-square"></i></a>
									<a href=""><i class="fa fa-twitter"></i></a>
									<a href=""><i class="fa fa-google-plus"></i></a>
									<a href=""><i class="fa fa-linkedin"></i></a>
									<a href=""><i class="fa fa-rss"></i></a>
							</div><!--TEAM SOCIAL END-->
					</div><!--TEAM BOXES END-->
				</div><!--COLS END-->
				<div class="col-md-4 col-sm-4 col-xs-12"><!--COLS START-->
					<div class="team-boxes not-animated" data-animate="fadeInUp" data-delay="0"><!--TEAM BOXES START-->
                        <div class="team-thumb overlay-image view-overlay"><!--TEAM THUMB START--><img src="http://herowp.com/demos/myway/wp-content/uploads/2014/09/Window-to-the-Soul-l-377x301.jpg" alt="Homepage"><div class="clear"></div>
						<div class="mask team_quote"><!--MASK START-->
                        						<div class="port-zoom-link height80"><!--port-zoom-link START-->
                                                	  <h2>"Imagination is more important then knowledge."</h2>
													  <p>Albert Einstein</p>
                                                </div><!--port-zoom-link END-->
                   							 </div><!--MASK END-->
                        </div><!--TEAM THUMB END-->
							<div class="team-info"><!--TEAM INFO START-->
									<h2>Jorge William Gatewood.</h2>
									<p>Programmer</p>
							</div><!--TEAM INFO END-->
							<div class="separator"></div>
							<div class="team-social"><!--TEAM SOCIAL START-->
									<a href=""><i class="fa fa-facebook-square"></i></a>
									<a href=""><i class="fa fa-twitter"></i></a>
									<a href=""><i class="fa fa-google-plus"></i></a>
									<a href=""><i class="fa fa-linkedin"></i></a>
									<a href=""><i class="fa fa-rss"></i></a>
							</div><!--TEAM SOCIAL END-->
					</div><!--TEAM BOXES END-->
				</div><!--COLS END-->
				<div class="col-md-4 col-sm-4 col-xs-12"><!--COLS START-->
					<div class="team-boxes not-animated" data-animate="fadeInUp" data-delay="0"><!--TEAM BOXES START-->
                        <div class="team-thumb overlay-image view-overlay"><!--TEAM THUMB START--><img src="http://herowp.com/demos/myway/wp-content/uploads/2014/09/photodune-2360845-astonishment-s-377x301.jpg" alt="Homepage"><div class="clear"></div>
						<div class="mask team_quote"><!--MASK START-->
                        						<div class="port-zoom-link height80"><!--port-zoom-link START-->
                                                	  <h2>"Imagination is more important then knowledge."</h2>
													  <p>Albert Einstein</p>
                                                </div><!--port-zoom-link END-->
                   							 </div><!--MASK END-->
                        </div><!--TEAM THUMB END-->
							<div class="team-info"><!--TEAM INFO START-->
									<h2>John Sean Vega</h2>
									<p>Advertiser</p>
							</div><!--TEAM INFO END-->
							<div class="separator"></div>
							<div class="team-social"><!--TEAM SOCIAL START-->
									<a href=""><i class="fa fa-facebook-square"></i></a>
									<a href=""><i class="fa fa-twitter"></i></a>
									<a href=""><i class="fa fa-google-plus"></i></a>
									<a href=""><i class="fa fa-linkedin"></i></a>
									<a href=""><i class="fa fa-rss"></i></a>
							</div><!--TEAM SOCIAL END-->
					</div><!--TEAM BOXES END-->
				</div><!--COLS END-->
	</div><!--TEAM WRAP END-->	
	
      </div><!--AQ BLOCK-509-37 END-->
	  
	  </div><!--CONTAINER END--></div><!--CUSTOM COLUMN BACKGROUND END-->


      </div><!--AQ BLOCK-509-35 END-->
	  
	  

	  
	  
</div><!--AQ TEMPLATE WRAPPER-509 END-->



<footer><!--FOOTER START-->
	
					<div class="big_phone_wrapper">
			<div class="container">
				<div class="col-md-12">
					<div class="big_phone">
						<p class="small_text">Wanna stai in touch?</p>
						<p class="big_text">
							<strong>0049</strong>
							-123-456-789						</p>
					</div>
				</div>
			</div>
		</div>
				
	<div class="footer-wrap"><!--FOOTER WRAP START-->
		<div class="container"><!--CONTAINER START-->
	
					<div class="left-logo">
				   <img class="footer-logo" src="http://herowp.com/demos/myway/wp-content/uploads/2014/08/logo.png" alt="Homepage" />   
			</div>
		 
		<div class="footer-box col-md-4" >
			
		<div class="footer-box-title"><!--FOOTER BOX TITLE START-->
									<h3>About us<span class="dotcolor">.</span></h3>
							<div class="footer-box-line2"></div>
		</div><!--FOOTER BOX TITLE END-->
			
			<div class="footer-box-content"><!--FOOTER BOX CONTENT START-->	
				
									<p>Chia direct trade VHS cred. Raw denim cool PBR&B Godard, organic XOXO. <br/><br/>Food truck <span>meggings</span> beard kitsch.</p>
								
				<ul>
					
											<li><i class="fa fa-map-marker"></i>Salt Lake City, UT 87234</li>
							
					
						
						<li><i class="fa fa-envelope"></i>awesome@example.com</li>
							
					
						
						<li><i class="fa fa-mobile"></i>(871) 321-6567</li>
							
					
											<li><i class="fa fa-clock-o"></i>Monday-Friday 08:00 - 18:00</li>
						
					
				</ul>
		</div><!--FOOTER BOX CONTENT END-->

	
			</div>
		
		<div class="footer-box col-md-4" >
				
		<h3>Latest news<span class="dotcolor">.</span></h3>
		
			<div class="footer-box-line2"></div>
		<div class="sidebar-posts-body">
							<div class="sidebar-posts-box">
					<div class="sidebar-post-thumb">
												<a href="http://herowp.com/demos/myway/enjoying-the-nature-in-a-weird-way-2/" title="Permalink to A little smile hasn&#8217;t killed anyone">
							<img src="http://herowp.com/demos/myway/wp-content/uploads/2015/03/02-100x100.jpg" alt="A little smile hasn&#8217;t killed anyone" class="categ-thumb"/>						</a>	
											
					</div>
					<div class="sidebar-posts">
						<h2><a href="http://herowp.com/demos/myway/enjoying-the-nature-in-a-weird-way-2/">A little smile hasn&#8217;t killed anyone<span class="dotcolor">.</span></a></h2>
						<p><span class="comments_sidebar"><i class="fa fa-comment-o"></i>0 Comments</span>  <i class="fa fa-calendar-o"></i>27 Mar 2015</p>
					</div>
				</div>
							<div class="sidebar-posts-box">
					<div class="sidebar-post-thumb">
												<a href="http://herowp.com/demos/myway/enjoying-the-nature-in-a-weird-way/" title="Permalink to Learn to do it yourself today and be cool">
							<img src="http://herowp.com/demos/myway/wp-content/uploads/2014/09/image-100x100.jpg" alt="Learn to do it yourself today and be cool" class="categ-thumb"/>						</a>	
											
					</div>
					<div class="sidebar-posts">
						<h2><a href="http://herowp.com/demos/myway/enjoying-the-nature-in-a-weird-way/">Learn to do it yourself today and be cool<span class="dotcolor">.</span></a></h2>
						<p><span class="comments_sidebar"><i class="fa fa-comment-o"></i>3 Comments</span>  <i class="fa fa-calendar-o"></i>27 Sep 2014</p>
					</div>
				</div>
				
		</div>
		
		</div>
		
		<div class="footer-box col-md-4" >
			            <div id="herowp_flickr_widget-6" class="herowp_flickr_widget">                <div class="footer-box-title">
							<h3>Flickr feed<span class="dotcolor">.</span></h3>
							<div class="footer-box-line2"></div>
						</div>
					<div class="clear"></div>
					<ul class="flickr"><li><a class="photo" href="http://www.flickr.com/photos/131198318@N07/" target="_blank"><img src="https://farm9.staticflickr.com/8744/16273181364_296ba11e67_n.jpg" alt="" /></a></li></ul>
			</div>            		</div>
		
	</div><!--CONTAINER END-->
</div><!--FOOTER WRAP END-->
	
</footer><!--FOOTER END-->
<div id="footer_copyright"><!--footer_copyright START-->
	
	<div class="container">
	
		
		
					<p>Copyright 2015 <strong>MYWAY</strong> Wordpress Theme. All rights reserved.</p>
				
		<div class="header-social footer-social"><!--SOCIAL START-->
						<ul>
							 								<li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i></a></li>
							 							 
							 								<li class="twitter"><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
							 							 
							 								<li class="google-plus"><a href="#" target="_blank"><i class="fa fa-google-plus"></i></a></li>
							 							 
							 								<li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							 							 
							 								<li class="rss"><a href="#" target="_blank"><i class="fa fa-rss"></i></a></li>
							 							 
							 								<li class="dribbble"><a href="#" target="_blank"><i class="fa fa-dribbble"></i></a></li>
							 							 
							 								<li class="youtube"><a href="#" target="_blank"><i class="fa fa-youtube"></i></a></li>
							 							 
							 								<li class="pinterest"><a href="#" target="_blank"><i class="fa fa-pinterest"></i></a></li>
							 						 
						</ul>
				</div><!--SOCIAL END--> 
	</div>
</div><!--footer_copyright END-->
</div><!--WRAP BOXED FULL END-->
<div class="revsliderstyles"><style type="text/css">.tp-caption.white_big{font-size:120px;font-weight:800;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-7px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.tp-caption.white_big_slim{font-size:120px;font-weight:100;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-7px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.tp-caption.white_medium_slim{font-size:42px;font-weight:100;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-2px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.tp-caption.white_medium_bold{font-size:42px;font-weight:800;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-2px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.tp-caption.white_small_slim{font-size:18px;line-height:24px;font-weight:100;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;letter-spacing:0px;max-width:50%;border-width:0px;border-color:rgb(0,0,0);border-style:none}.white_big{font-size:120px;font-weight:800;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-7px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.white_big_slim{font-size:120px;font-weight:100;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-7px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.white_medium_slim{font-size:42px;font-weight:100;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-2px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.white_medium_bold{font-size:42px;font-weight:800;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;text-transform:uppercase;letter-spacing:-2px;border-width:0px;border-color:rgb(0,0,0);border-style:none}.white_small_slim{font-size:18px;line-height:24px;font-weight:100;color:#ffffff;text-decoration:none;background-color:transparent;text-shadow:none;letter-spacing:0px;max-width:50%;border-width:0px;border-color:rgb(0,0,0);border-style:none}</style>
</div><link rel='stylesheet' id='herowp_builder_font_opensans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans:100,300,400,400italic,500,600,700,700italic&#038;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese' media='all' />
<script type='text/javascript' src='js/aqpb-view.js'></script>
<script type='text/javascript' src='js/comment-reply.min.js'></script>
<script type='text/javascript' src='js/jquery.form.min.js'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"http:\/\/herowp.com\/demos\/myway\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ..."};
/* ]]> */
</script>
<script type='text/javascript' src='js/scripts.js'></script>
<script type='text/javascript' src='js/jquery-ui.min.js'></script>
<script type='text/javascript' src='js/lightbox.min.js'></script>
<script type='text/javascript' src='js/html5lightbox.js'></script>
<script type='text/javascript' src='js/touchTouch.jquery.js'></script>
<script type='text/javascript' src='js/jquery.counterup.min.js'></script>
<script type='text/javascript' src='js/waypoints.min.js'></script>
<script type='text/javascript' src='js/jquery.flexslider.js'></script>
<script type='text/javascript' src='js/modernizr.js'></script>
<script type='text/javascript' src='js/jquery.mousewheel.js'></script>
<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/jquery.stellar.js'></script>
<script type='text/javascript' src='js/jquery.parallax-1.1.3.js'></script>
<script type='text/javascript' src='js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='js/jquery.appear.js'></script>
<script type='text/javascript' src='js/jquery.mb.YTPlayer.js'></script>
<script type='text/javascript' src='js/jquery-asPieProgress.js'></script>
<script type='text/javascript' src='js/script.js'></script>
<script type='text/javascript' src='js/jquery.smoothscroll.js'></script>
<script type='text/javascript' src='js/script-animate.js'></script>
<script type='text/javascript' src='js/menu_functions.js'></script>
        <script type="text/javascript" src="cordova.js"></script>
        <script type="text/javascript" src="js/index.js"></script>
        <script type="text/javascript">
            app.initialize();
        </script>
		<script type="text/javascript">
			function ajaxcall(){
				 var ajaxRequest;  // The variable that makes Ajax possible!
	
				 try{
				   // Opera 8.0+, Firefox, Safari
				   ajaxRequest = new XMLHttpRequest();
				 }catch (e){
				   // Internet Explorer Browsers
				   try{
					  ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
				   }catch (e) {
					  try{
						 ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					  }catch (e){
						 // Something went wrong
						 alert("Your browser broke!");
						 return false;
					  }
				   }
				 }
				 // Create a function that will receive data 
				 // sent from the server and will update
				 // div section in the same page.
				 ajaxRequest.onreadystatechange = function(){
				   if(ajaxRequest.readyState == 4){
					  var ajaxDisplay = document.getElementById('papan');
					  ajaxDisplay.innerHTML = ajaxRequest.responseText;
				   }
				 }
				 // Now get the value from user and pass it to
				 // server script.
				 ajaxRequest.open("GET", "http://localhost/moj/ajax/index_details.php"  
											  , true);
				 ajaxRequest.send(null);
			}
		</script>
<script language="javascript">
document.onmousedown=disableclick;
status="Right Click Disabled";
function disableclick(event)
{
  if(event.button==2)
   {
     alert(status);
     return false;    
   }
}
</script>

    </body>
</html>
