<?php include('config.php');
?>

<div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a class="active" href="dashboard.php">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
				<!-- <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-book"></i>
                        <span>User Details</span>
                    </a>
                    <ul class="sub">
                        <li><a href="create_user_admin.php">Create user</a></li>
                        <li><a href="view_user_admin.php">View user</a></li> 
                    </ul>
                </li> -->
				<!-- <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-laptop"></i>
                        <span>Registration Template</span>
                    </a>
                    <ul class="sub">
						<li><a href="create_registration_image.php">Create Details</a></li>
						<li><a href="registration_image.php">View Details</a></li>
                    </ul>
                </li> -->

				<li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-laptop"></i>
                        <span>Catagory</span>
                    </a>
                    <ul class="sub">
                        <li><a href="create_catagory.php">Create Catagory</a></li>
						<li><a href="view_main_catagory.php">View Main Catagory</a></li>
						<li><a href="view_catagory.php">View Catagory</a></li>
                    </ul>
                </li>
				<li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-book"></i>
                        <span>Photography</span>
                    </a>
                    <ul class="sub">
                        <li><a href="view_catagory_photogarhy.php">Create Photography</a></li>
                        <li><a href="view_photography.php">View Photography</a></li> 
                    </ul>
                </li>
				<li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-book"></i>
                        <span>Home Page Slider</span>
                    </a>
                    <ul class="sub">
                        <li><a href="create_slider.php">Create Slider</a></li>
                        <li><a href="view_slider.php">View Slider</a></li> 
                    </ul>
                </li>
				<li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-book"></i>
                        <span>Price Details</span>
                    </a>
                    <ul class="sub">
                        <li><a href="view_price_table.php">View Price Details</a></li> 
                    </ul>
                </li>
            </ul>  
		</div>