/* SHARED VARS */
/*global jQuery:false */
var touch = false;
var j = jQuery.noConflict();
// handles Animate
function dataAnimate() {

  "use strict";
  j('[data-animate]').each(function (){
  
  var jtoAnimateElement = j(this);
    
  var toAnimateDelay = j(this).attr('data-delay');
    
  var toAnimateDelayTime = 0;
    
  if( toAnimateDelay ) { toAnimateDelayTime = Number( toAnimateDelay ); } else { toAnimateDelayTime = 200; }
    
  if( !jtoAnimateElement.hasClass('animated') ) {
      
      jtoAnimateElement.addClass('not-animated');
      
      var elementAnimation = jtoAnimateElement.attr('data-animate');
      
      jtoAnimateElement.appear(function () {
        
      setTimeout(function () {
          jtoAnimateElement.removeClass('not-animated').addClass( elementAnimation + ' animated');
        }, toAnimateDelayTime);
        
      },{accX: 0, accY: -150},'easeInCubic');
      
    }
    
  });
}

//Intitialize animations
jQuery(document).ready(function () {
	if(touch === false)
	{
		dataAnimate();
	}
});